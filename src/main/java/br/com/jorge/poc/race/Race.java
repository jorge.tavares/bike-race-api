package br.com.jorge.poc.race;

import br.com.jorge.poc.core.RaceModality;
import br.com.jorge.poc.integration.race.RaceCreateEvent;
import br.com.jorge.poc.integration.race.RaceUpdateEvent;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.AUTO;
import static lombok.AccessLevel.PRIVATE;

@Entity
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = PRIVATE)
public class Race {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @Enumerated(STRING)
    private RaceModality modality;

    private String description;

    public static Race of(RaceCreateEvent raceCreateEvent) {
        return Race.builder()
                .id(raceCreateEvent.getId())
                .description(raceCreateEvent.getDescription())
                .build();
    }

    public static Race of(RaceUpdateEvent potatoCreateEvent) {
        return Race.builder()
                .id(potatoCreateEvent.getId())
                .description(potatoCreateEvent.getDescription())
                .build();
    }
}
