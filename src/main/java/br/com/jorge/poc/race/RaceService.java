package br.com.jorge.poc.race;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RaceService {
    Race create(Race race);
    Race update(Race race, Integer id);
    Race findById(Integer id);
    String justLog(Race race, String description);
    Page<Race> findAll(Pageable pageable);
}
