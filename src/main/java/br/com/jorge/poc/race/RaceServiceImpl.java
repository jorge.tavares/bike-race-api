package br.com.jorge.poc.race;

import br.com.jorge.poc.core.exception.ConflictException;
import br.com.jorge.poc.core.exception.ResourceNotFoundException;
import br.com.jorge.poc.integration.race.RaceCreateEvent;
import br.com.jorge.poc.integration.race.RaceUpdateEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
class RaceServiceImpl implements RaceService {

    private final RaceRepository raceRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    @Transactional
    public Race create(Race race) {
        raceRepository.findByDescription(race.getDescription())
                .ifPresent(p -> { throw new ConflictException("Race already exists"); });
        Race saved = raceRepository.save(race);
        applicationEventPublisher.publishEvent(RaceCreateEvent.of(race));
        return saved;
    }

    @Override
    @Transactional
    public Race update(Race race, Integer id) {
        Race updated = raceRepository.save(findById(id).toBuilder()
                .id(id)
                .description(race.getDescription())
                .build());
        applicationEventPublisher.publishEvent(RaceUpdateEvent.of(race));
        return updated;
    }

    @Override
    public Race findById(Integer id) {
        return raceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Race not found for " + id));
    }

    @Override
    public Page<Race> findAll(Pageable pageable) {
        return raceRepository.findAll(pageable);
    }

    @Override
    public String justLog(Race race, String description) {
        String info = "Race " + race.toString() + " Description " + description;
        log.info(info);
        return info;
    }
}
