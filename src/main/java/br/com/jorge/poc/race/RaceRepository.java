package br.com.jorge.poc.race;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

interface RaceRepository extends JpaRepository<Race, Integer> {
    Optional<Race> findByDescription(String description);
}
