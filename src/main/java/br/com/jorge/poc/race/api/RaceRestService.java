package br.com.jorge.poc.race.api;

import br.com.jorge.poc.race.Race;
import br.com.jorge.poc.race.RaceService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/bike-races")
class RaceRestService {

    private final RaceService bikeRaceService;

    @PostMapping
    @ResponseStatus(CREATED)
    Race create(@RequestBody Race race) {
        return bikeRaceService.create(race);
    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    Race update(@RequestBody Race race, @PathVariable Integer id) {
        return bikeRaceService.update(race, id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    Race getById(@PathVariable Integer id) {
        return bikeRaceService.findById(id);
    }

    @GetMapping
    @ResponseStatus(OK)
    Page<Race> getAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                      @RequestParam(defaultValue = "20", required = false) Integer size) {
        return bikeRaceService.findAll(PageRequest.of(page, size));
    }
}
