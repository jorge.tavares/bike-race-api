package br.com.jorge.poc.integration.race;

import br.com.jorge.poc.core.exception.BusinessLogicException;
import br.com.jorge.poc.race.Race;
import br.com.jorge.poc.race.RaceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ff4j.FF4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static br.com.jorge.poc.configuration.Features.FORCE_QUEUE_RETRY;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(RaceChannel.class)
public class RaceConsumer {

    private final FF4j ff4j;
    private final RaceService bikeRaceService;

    @StreamListener(target = RaceChannel.INPUT , condition = "headers['event-type']=='RaceCreateEvent'")
    public void onReceiveMessage(@Payload RaceCreateEvent raceCreateEvent) {
        if (ff4j.check(FORCE_QUEUE_RETRY.name())) {
            throw new BusinessLogicException("Fail to consumes a race create event");
        } else {
            bikeRaceService.justLog(Race.of(raceCreateEvent), raceCreateEvent.getEventType());
        }
    }

    @StreamListener(target = RaceChannel.INPUT , condition = "headers['event-type']=='RaceUpdateEvent'")
    public void onReceiveMessage(@Payload RaceUpdateEvent potatoCreateEvent) {
        bikeRaceService.justLog(Race.of(potatoCreateEvent), potatoCreateEvent.getEventType());
    }

}
