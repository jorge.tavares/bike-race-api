package br.com.jorge.poc.integration.race;

import lombok.AllArgsConstructor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@AllArgsConstructor
@EnableBinding(RaceChannel.class)
public class RaceProducer {

    private final RaceChannel raceChannel;

    @TransactionalEventListener(RaceCreateEvent.class)
    public void produce(RaceCreateEvent raceCreateEvent) {
        raceChannel.output().send(raceCreateEvent.message());
    }

}
