package br.com.jorge.poc.integration.race;

import br.com.jorge.poc.core.DomainEvent;
import br.com.jorge.poc.core.RaceModality;
import br.com.jorge.poc.race.Race;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RaceCreateEvent implements DomainEvent {

    private Integer id;
    private RaceModality modality;
    private String description;

    @Override
    public String getEventType() {
        return this.getClass().getSimpleName();
    }

    public static RaceCreateEvent of(Race race) {
        return RaceCreateEvent.builder()
                .id(race.getId())
                .modality(race.getModality())
                .description(race.getDescription())
                .build();
    }

    public Message<RaceCreateEvent> message() {
        return MessageBuilder.withPayload(this)
                .setHeader("event-type", this.getEventType())
                .build();
    }
}
