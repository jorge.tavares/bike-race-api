package br.com.jorge.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BikeRaceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BikeRaceApiApplication.class, args);
	}

}
