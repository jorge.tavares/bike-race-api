package br.com.jorge.poc.core;

public enum RaceModality {
    MTB,
    ROAD
}
