package br.com.jorge.poc.core;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface DomainEvent {
    @JsonIgnore
    String getEventType();
}
