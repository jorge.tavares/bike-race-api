package br.com.jorge.poc.race

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.core.exception.ConflictException
import br.com.jorge.poc.core.exception.ResourceNotFoundException
import br.com.jorge.poc.integration.race.RaceCreateEvent
import br.com.jorge.poc.integration.race.RaceUpdateEvent
import br.com.six2six.fixturefactory.Fixture
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared

import static br.com.jorge.poc.templates.FixtureTemplate.MTB
import static br.com.jorge.poc.templates.FixtureTemplate.VALID

class RaceServiceImplTest extends AbstractTestSpecification {

    @Shared
    RaceRepository raceRepository

    @Shared
    ApplicationEventPublisher applicationEventPublisher

    RaceService raceService

    def setup() {
        raceRepository = Mock(RaceRepository)
        applicationEventPublisher = Mock(ApplicationEventPublisher)
        raceService = new RaceServiceImpl(raceRepository, applicationEventPublisher)
    }

    def 'should create a nonexistent race' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        Race result = raceService.create(race)

        then:
        1 * raceRepository.findByDescription(race.getDescription()) >> Optional.empty()
        1 * raceRepository.save(race) >> race
        1 * applicationEventPublisher.publishEvent(_ as RaceCreateEvent)

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should thrown a conflict exception when tries to create a existent race' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        raceService.create(race)

        then:
        1 * raceRepository.findByDescription(race.getDescription()) >> Optional.of(race)

        and:
        ConflictException e = thrown(ConflictException)

        and:
        e.message == 'Race already exists'
    }

    def 'should update a race with success' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())
        Integer id = race.getId()

        when:
        Race result = raceService.update(race, id)

        then:
        1 * raceRepository.findById(id) >> Optional.of(race)
        1 * raceRepository.save(race) >> race
        1 * applicationEventPublisher.publishEvent(_ as RaceUpdateEvent)

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should throw a resource not found exception when the race does not exists' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())
        Integer id = race.getId()

        when:
        raceService.update(race, id)

        then:
        1 * raceRepository.findById(id) >> Optional.empty()

        and:
        ResourceNotFoundException e = thrown(ResourceNotFoundException)

        and:
        e.message == 'Race not found for ' + id
    }

    def 'should retrieve just one race with success' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        Race result = raceService.findById(race.getId())

        then:
        1 * raceRepository.findById(race.getId()) >> Optional.of(race)

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should throw a resource not found exception when race id does not exists' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        raceService.findById(race.getId())

        then:
        1 * raceRepository.findById(race.getId()) >> Optional.empty()

        and:
        ResourceNotFoundException e = thrown(ResourceNotFoundException)

        and:
        e.message == 'Race not found for ' + race.getId()
    }

    def 'should retrieve pages of race with success' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        Page<Race> result = raceService.findAll(firstPage)

        then:
        1 * raceRepository.findAll(firstPage) >> new PageImpl<>(Arrays.asList(race))

        and:
        result.totalElements == 1
        result.content.get(0).id == race.id
        result.content.get(0).modality == race.modality
        result.content.get(0).description == race.description
    }

    def 'should just log a mtb race received with description' () {
        given:
        Race race = Fixture.from(Race.class).gimme(MTB.name())

        when:
        String result = raceService.justLog(race, race.getDescription())

        then:
        result == 'Race ' + race.toString() + ' Description ' + race.getDescription()
    }

}
