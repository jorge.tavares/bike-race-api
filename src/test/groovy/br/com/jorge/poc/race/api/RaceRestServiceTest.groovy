package br.com.jorge.poc.race.api

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.race.Race
import br.com.jorge.poc.race.RaceService
import br.com.six2six.fixturefactory.Fixture
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared

import static br.com.jorge.poc.templates.FixtureTemplate.VALID

class RaceRestServiceTest extends AbstractTestSpecification {

    @Shared
    RaceService raceService

    RaceRestService raceRestService

    def setup() {
        raceService = Mock(RaceService)
        raceRestService = new RaceRestService(raceService)
    }

    def 'should create a race with success'() {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        Race result = raceRestService.create(race)

        then:
        1 * raceService.create(race) >> race

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should update a race with success'() {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        Race result = raceRestService.update(race, race.getId())

        then:
        1 * raceService.update(race, race.getId()) >> race

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should retrieve just one race by id with success'() {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())

        when:
        Race result = raceRestService.getById(race.getId())

        then:
        1 * raceService.findById(race.getId()) >> race

        and:
        result.id == race.id
        result.modality == race.modality
        result.description == race.description
    }

    def 'should retrieve first race page with success' () {
        given:
        Race race = Fixture.from(Race.class).gimme(VALID.name())
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        Page<Race> result = raceRestService.getAll(0, 1)

        then:
        1 * raceService.findAll(firstPage) >> new PageImpl<>(Arrays.asList(race))

        and:
        result.totalElements == 1
        result.content.get(0).id == race.id
        result.content.get(0).modality == race.modality
        result.content.get(0).description == race.description
    }
}
