package br.com.jorge.poc.integration.race

import br.com.jorge.poc.AbstractTestSpecification
import br.com.six2six.fixturefactory.Fixture
import org.junit.Ignore
import spock.lang.Shared

import static br.com.jorge.poc.templates.FixtureTemplate.VALID

@Ignore
class RaceProducerTest extends AbstractTestSpecification {

    @Shared
    RaceChannel raceChannel

    RaceProducer raceProducer

    def setup() {
        raceChannel = Mock(RaceChannel)
        raceProducer = new RaceProducer(raceChannel)
    }

    def 'should produce a race create event with success' () {
        given:
        RaceCreateEvent raceCreateEvent = Fixture.from(RaceCreateEvent.class).gimme(VALID.name());

        when:
        raceProducer.produce(raceCreateEvent)

        then:
        1 * raceChannel.output().send(_ as RaceCreateEvent)
    }

}
