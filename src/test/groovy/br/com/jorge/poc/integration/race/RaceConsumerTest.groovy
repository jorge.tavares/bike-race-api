package br.com.jorge.poc.integration.race

import br.com.jorge.poc.AbstractTestSpecification
import br.com.jorge.poc.core.exception.BusinessLogicException
import br.com.jorge.poc.race.Race
import br.com.jorge.poc.race.RaceService
import br.com.six2six.fixturefactory.Fixture
import org.ff4j.FF4j
import spock.lang.Shared

import static br.com.jorge.poc.configuration.Features.FORCE_QUEUE_RETRY
import static br.com.jorge.poc.templates.FixtureTemplate.VALID
import static java.lang.Boolean.FALSE
import static java.lang.Boolean.TRUE

class RaceConsumerTest extends AbstractTestSpecification {

    @Shared
    RaceService raceService

    @Shared
    FF4j ff4j

    RaceConsumer raceConsumer

    def setup() {
        ff4j = Mock(FF4j)
        raceService = Mock(RaceService)
        raceConsumer = new RaceConsumer(ff4j, raceService)
    }

    def 'should consumes a race created event when force retry feature is off' () {
        given:
        RaceCreateEvent raceCreateEvent = Fixture.from(RaceCreateEvent.class).gimme(VALID.name())

        when:
        raceConsumer.onReceiveMessage(raceCreateEvent)

        then:
        1 * ff4j.check(FORCE_QUEUE_RETRY.name()) >> FALSE
        1 * raceService.justLog(Race.of(raceCreateEvent), raceCreateEvent.getEventType())
    }

    def 'should throw a business exception when force retry feature is on' () {
        given:
        RaceCreateEvent raceCreateEvent = Fixture.from(RaceCreateEvent.class).gimme(VALID.name())

        when:
        raceConsumer.onReceiveMessage(raceCreateEvent)

        then:
        1 * ff4j.check(FORCE_QUEUE_RETRY.name()) >> TRUE

        and:
        BusinessLogicException e = thrown(BusinessLogicException)

        and:
        e.message == 'Fail to consumes a race create event'
    }

    def 'should consumes a race update event when force retry feature is off' () {
        given:
        RaceUpdateEvent raceUpdateEvent = Fixture.from(RaceUpdateEvent.class).gimme(VALID.name())

        when:
        raceConsumer.onReceiveMessage(raceUpdateEvent)

        then:
        1 * raceService.justLog(Race.of(raceUpdateEvent), raceUpdateEvent.getEventType())
    }

}
