package br.com.jorge.poc


import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader
import spock.lang.Specification

class AbstractTestSpecification extends Specification {
    def setupSpec() {
        FixtureFactoryLoader.loadTemplates("br.com.jorge.poc.templates")
    }
}
