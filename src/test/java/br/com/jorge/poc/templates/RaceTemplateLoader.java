package br.com.jorge.poc.templates;

import br.com.jorge.poc.core.RaceModality;
import br.com.jorge.poc.integration.race.RaceCreateEvent;
import br.com.jorge.poc.race.Race;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import static br.com.jorge.poc.templates.FixtureTemplate.MTB;
import static br.com.jorge.poc.templates.FixtureTemplate.ROAD;
import static br.com.jorge.poc.templates.FixtureTemplate.VALID;

public class RaceTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(Race.class).addTemplate(VALID.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", random(RaceModality.MTB, RaceModality.ROAD));
            add("description", random("A short MTB race", "A long ROAD race", "A XCO MTB race"));
        }});

        Fixture.of(Race.class).addTemplate(MTB.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", RaceModality.MTB);
            add("description", random("A short MTB race", "A XCO MTB race"));
        }});

        Fixture.of(Race.class).addTemplate(ROAD.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", RaceModality.ROAD);
            add("description", "A long ROAD race");
        }});
    }
}
