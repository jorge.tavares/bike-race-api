package br.com.jorge.poc.templates;

import br.com.jorge.poc.core.RaceModality;
import br.com.jorge.poc.integration.race.RaceCreateEvent;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import java.util.Random;
import java.util.UUID;

import static br.com.jorge.poc.templates.FixtureTemplate.MTB;
import static br.com.jorge.poc.templates.FixtureTemplate.ROAD;
import static br.com.jorge.poc.templates.FixtureTemplate.VALID;

public class RaceCreateEventTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(RaceCreateEvent.class).addTemplate(VALID.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", random(RaceModality.MTB, RaceModality.ROAD));
            add("description", random("A short MTB race", "A long ROAD race", "A XCO MTB race"));
        }});

        Fixture.of(RaceCreateEvent.class).addTemplate(MTB.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", RaceModality.MTB);
            add("description", random("A short MTB race", "A XCO MTB race"));
        }});

        Fixture.of(RaceCreateEvent.class).addTemplate(ROAD.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", RaceModality.ROAD);
            add("description", "A long ROAD race");
        }});
    }
}
