package br.com.jorge.poc.templates;

import br.com.jorge.poc.core.RaceModality;
import br.com.jorge.poc.integration.race.RaceUpdateEvent;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import static br.com.jorge.poc.templates.FixtureTemplate.VALID;

public class RaceUpdateEventTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(RaceUpdateEvent.class).addTemplate(VALID.name(), new Rule() {{
            add("id", random(Integer.class, range(0, 100)));
            add("modality", random(RaceModality.MTB, RaceModality.ROAD));
            add("description", random("A short MTB race", "A long ROAD race", "A XCO MTB race"));
        }});
    }
}
